//
//  WavGenerator.cpp
//  AudioValidator
//
//  Created by Tom on 2017. 4. 24..
//  Copyright © 2017년 tom. All rights reserved.
//

#include "WavGenerator.h"
#include <chrono>

WavGenerator::WavGenerator():
_sumOfBufferLength(0)
{
    
}

WavGenerator::~WavGenerator() {
    
}

int WavGenerator::wavHeaderGenerator(WavHeader* wavHeader, int channels, int sampleRate, int sumOfBufferLength)
{
    printf("Duration     : %f", (float)sumOfBufferLength/sampleRate);
    
    // RIFF chunk descriptor
    wavHeader->ChunkID[0] = 'R';
    wavHeader->ChunkID[1] = 'I';
    wavHeader->ChunkID[2] = 'F';
    wavHeader->ChunkID[3] = 'F';
    wavHeader->Format[0] = 'W';
    wavHeader->Format[1] = 'A';
    wavHeader->Format[2] = 'V';
    wavHeader->Format[3] = 'E';
    // fmt sub-chunk
    wavHeader->Subchunk1ID[0] = 'f';
    wavHeader->Subchunk1ID[1] = 'm';
    wavHeader->Subchunk1ID[2] = 't';
    wavHeader->Subchunk1ID[3] = ' ';
    
    wavHeader->Subchunk1Size = 16;
    wavHeader->AudioFormat = 1;
    wavHeader->NumChannels = channels;
    wavHeader->SampleRate = sampleRate;
    wavHeader->ByteRate = 2 * wavHeader->NumChannels * wavHeader->SampleRate;
    wavHeader->BlockAlign = 2;
    wavHeader->BitsPerSample = 16;
    // data sub-chunk
    wavHeader->Subchunk2ID[0] = 'd';
    wavHeader->Subchunk2ID[1] = 'a';
    wavHeader->Subchunk2ID[2] = 't';
    wavHeader->Subchunk2ID[3] = 'a';
    
    wavHeader->Subchunk2Size = sumOfBufferLength * wavHeader->NumChannels * wavHeader->BitsPerSample / 8; // samplesCount * NumChannels * BitsPerSample
    wavHeader->ChunkSize     = 4 + (8 + wavHeader->Subchunk1Size) + (8 + wavHeader->Subchunk2Size);  // 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
    
    return 1;
}

int WavGenerator::openWavFile(const char* folderPath, int channels, int sampleRate)
{
    int result = 0;
    _channels = channels;
    _sampleRate = sampleRate;
    _pWavFile = fopen(folderPath, "wb");
    
    if(_pWavFile == nullptr){
        printf("Wrong wave file path : %s \n",folderPath);
        result = 1;
    }else{
        printf("------------- Open Success -----------");
        printf("File path : %s",folderPath);
        printf("channels          : %d", channels);
        printf("sampleRate        : %d", sampleRate);
        time_t timev = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        printf("Current Time      : %s \n", std::ctime(&timev));
        
        fwrite(&_wavHeader, sizeof(WavHeader), 1, _pWavFile);    // write temporary wav header
    }
    
    return result;
}

int WavGenerator::writeAudioBuffer(short* audioBuffer, int bufferLenght)
{
    int result = 0;
    
    int writeLen = fwrite(audioBuffer, sizeof(short), bufferLenght, _pWavFile);
    
    if(writeLen != bufferLenght){
        printf("Error writeAudioBuffer!");
        result = 1;
    }else{
        _sumOfBufferLength += bufferLenght/_channels;
    }
    
    return result;
}

int WavGenerator::closeWavFile()
{
    int result = 0;
    
    wavHeaderGenerator(&_wavHeader, _channels, _sampleRate, _sumOfBufferLength);
    fseek(_pWavFile, 0, SEEK_SET);
    
    int writeLen = fwrite(&_wavHeader, sizeof(WavHeader), 1, _pWavFile);
    
    if(writeLen != 1){
        printf("Error closeWavFile!");
        result = 1;
    }else{
        fclose(_pWavFile);
        printf("------------- Successfully done -----------");
    }
    
    return result;
}

int WavGenerator::openTextFile(const char* folderPath)
{
    int result = 0;
    _pTextFile = fopen(folderPath, "wb");
    
    if(_pTextFile == nullptr){
        printf("Wrong wav file path : %s \n",folderPath);
        result = 1;
    }else{
        printf("------------- Open Success -----------");
        printf("File path : %s",folderPath);
        time_t timev = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
        printf("Current Time      : %s \n", std::ctime(&timev));
    }
    
    return result;
}

int WavGenerator::writeHeadTrackingInfo(float *listenerForwardVector,
                                                         float *listenerUpVeoctor,
                                                         float *listenerPositionVector){
    
    int result = 0;
    
    if(_pTextFile == nullptr){
        printf("Error writeText!");
        result = 1;
    }else{
        fprintf(_pTextFile, "%f,%f,%f,%f,%f,%f,%f,%f,%f\n",
                listenerForwardVector[0],listenerForwardVector[1],listenerForwardVector[2],
                listenerUpVeoctor[0],listenerUpVeoctor[1],listenerUpVeoctor[2],
                listenerPositionVector[0],listenerPositionVector[1],listenerPositionVector[2]);
    }
    
    return  result;
}

int WavGenerator::closeTextFile(){
    int result = 0;
    
    if(_pTextFile == nullptr){
        printf("Error closeTextFile!");
        result = 1;
    }else{
        fclose(_pTextFile);
    }
    
    return  result;
}