//
//  Utilities.hpp
//  AudioValidator
//
//  Created by Tom on 2017. 4. 24..
//  Copyright © 2017년 tom. All rights reserved.
//

#ifndef Utilities_h
#define Utilities_h

#include <stdio.h>
#include <string>
class Utilities{
private:
    
public:
    Utilities();
    ~Utilities();
    std::string createFileNameWithCurrentTime();
};

#endif /* Utilities_h */
