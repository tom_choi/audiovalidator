//
//  AudioValidator.hpp
//  AudioValidator
//
//  Created by Tom on 2017. 4. 24..
//  Copyright © 2017년 tom. All rights reserved.
//

#ifndef AudioValidator_hpp
#define AudioValidator_hpp

#include "WavGenerator.h"
#include "Utilities.h"

class AudioValidator
{
private:
    
public:
    AudioValidator();
    ~AudioValidator();
    
    WavGenerator waveGenerator;
    Utilities utilManager;
};

#endif /* AudioValidator_hpp */
