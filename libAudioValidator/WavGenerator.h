//
//  WavGenerator.hpp
//  AudioValidator
//
//  Created by Tom on 2017. 4. 24..
//  Copyright © 2017년 tom. All rights reserved.
//

#ifndef WavGenerator_h
#define WavGenerator_h

#include <stdio.h>

class WavGenerator{

    struct WavHeader{
        // RIFF chunk descriptor
        char ChunkID[4];                // "RIFF"
        int ChunkSize;                  // 4 + (8 + SubChunk1Size) + (8 + SubChunk2Size)
        char Format[4];                 // "WAVE"
        // fmt sub-chunk
        char Subchunk1ID[4];            // "fmt "
        int Subchunk1Size;              // bytes remaining in subchunk, 16 if uncompressed
        short AudioFormat;              // 1 = uncompressed
        short NumChannels;              // mono or stereo
        int SampleRate;
        int ByteRate;                   // == SampleRate * NumChannels * BitsPerSample/8
        short BlockAlign;               // == NumChannels * BitsPerSample/8
        short BitsPerSample;
        // data sub-chunk
        char Subchunk2ID[4];            // "data"
        int Subchunk2Size;              // == samplesCount * NumChannels * BitsPerSample/8
    };
    
    WavHeader _wavHeader;
    int _sumOfBufferLength;
    int _channels;
    int _sampleRate;
    FILE* _pWavFile;
    
    FILE* _pTextFile;
    
    
    int wavHeaderGenerator(WavHeader* wavHeader, int channels, int sampleRate, int sumOfBufferLength);
public:
    WavGenerator();
    ~WavGenerator();
    int openWavFile(const char* folderPath, int channels, int sampleRate);
    int writeAudioBuffer(short* audioBuffer, int bufferLenght);
    int closeWavFile();
    
    int openTextFile(const char* folderPath);
    int writeHeadTrackingInfo(float *listenerForwardVector,
                              float *listenerUpVeoctor,
                              float *listenerPositionVector);
    int closeTextFile();
};


#endif /* WavGenerator_h */
